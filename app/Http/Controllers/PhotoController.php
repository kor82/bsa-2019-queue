<?php

namespace App\Http\Controllers;

use App\Entities\Photo;
use Illuminate\Http\Request;
use App\Jobs\CropJob;

class PhotoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'photo' => ['required', 'image']
        ]);

        $originalName = $request->file('photo')->getClientOriginalName();

        $path = $request->file('photo')
                        ->storeAs('/images/'.$request->user()->id, $originalName);

        $photo = Photo::create([
            'user_id' => auth()->id(),
            'original_photo' => $path,
            'status' => 'UPLOADED'
        ]);        

        CropJob::dispatch($photo);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Entities\Photo  $photo
     * @return \Illuminate\Http\Response
     */
    public function show(Photo $photo)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Entities\Photo  $photo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Photo $photo)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Entities\Photo  $photo
     * @return \Illuminate\Http\Response
     */
    public function destroy(Photo $photo)
    {
        //
    }
}
