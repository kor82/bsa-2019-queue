<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Services\Contracts\PhotoService;
use App\Entities\Photo;
use App\Notifications\ImageProcessedNotification;
use App\Notifications\ImageProcessingFailedNotification;
use App\Entities\User;

class CropJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Photo instance
     * 
     * @var App\Entities\Photo
     */
    public $photo;

    /**
     * User instance
     * 
     * @var App\Entities\User
     */
    public $user;    

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Photo $photo)
    {
        $this->photo = $photo;
        $this->user = User::find($this->photo->user_id);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(PhotoService $photoservice)
    {
        $this->photo->update([
            'status' => 'PROCESSING'
        ]);

        $path_100_100 = $photoservice->crop($this->photo->original_photo, 100, 100);
        $path_150_150 = $photoservice->crop($this->photo->original_photo, 150, 150);
        $path_250_250 = $photoservice->crop($this->photo->original_photo, 250, 250);

        $this->photo->update([
            'photo_100_100' => $path_100_100, 
            'photo_150_150' => $path_150_150, 
            'photo_250_250' => $path_250_250,
            'status' => 'SUCCESS'
        ]);

        $this->user->notify(new ImageProcessedNotification($this->photo));
    }

    /**
     *
     * @param  Exception  $exception
     * @return void
     */
    public function failed()
    {
        $this->photo->update([
            'status' => 'FAIL'
        ]);
        $this->user->notify(new ImageProcessingFailedNotification($this->photo));        
    }    
}
