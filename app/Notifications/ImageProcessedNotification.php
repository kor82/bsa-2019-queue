<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use App\Entities\Photo;
use Illuminate\Support\Facades\Storage;
use Illuminate\Notifications\Messages\BroadcastMessage;

class ImageProcessedNotification extends Notification
{
    use Queueable;

    /**
     * Photo instance
     * 
     * @var App\Entities\Photo
     */
    public $photo;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Photo $photo)
    {
        $this->photo = $photo;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail', 'broadcast'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line("Dear {$notifiable->name},")
                    ->line("Photos have been successfully uploaded and processed.")
                    ->line("Here are links to the images:")
                    ->line('100 x 100: '.url(Storage::url($this->photo->photo_100_100)))
                    ->line('150 x 150: '.url(Storage::url($this->photo->photo_150_150)))
                    ->line('250 x 250: '.url(Storage::url($this->photo->photo_250_250)))
                    ->line("Thanks!");
    }

    /**
     * Get the broadcastable representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return BroadcastMessage
     */
    public function toBroadcast($notifiable)
    {
        return new BroadcastMessage([
            'photo_100_100' => Storage::url($this->photo->photo_100_100),
            'photo_150_150' => Storage::url($this->photo->photo_150_150),
            'photo_250_250' => Storage::url($this->photo->photo_250_250),
            'status' => $this->photo->status,
        ]);
    }

}
