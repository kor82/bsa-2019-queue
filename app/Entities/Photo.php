<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Photo extends Model
{
    protected $fillable = [
    	'user_id', 'original_photo', 'photo_100_100', 'photo_150_150', 'photo_250_250', 'status'
    ];

    /**
     * Get the status in lower case.
     *
     * @param  string  $value
     * @return string
     */
    public function getStatusAttribute($value)
    {
        return mb_strtolower($value);
    }


}
